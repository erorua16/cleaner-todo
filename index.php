<!-- Establish connection to php my admin -->
<?php require "connection.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<h1>TO DO LIST</h1>
<div class="add_to_table">
    <a href="add_to_table.php"><button>Add to story</button></a>
</div>
<!----------------------------------------------------------------SHOW ALL ITEMS IN TO DO------------------------------------------------------------------------------->
<h2>TABLE RESULTS</h2>
<?php
// Query to get information from the database
$sql = "SELECT * FROM list_of_todo";
$result = $mysqli->query($sql);
?>
<table>
<tr>
    <th>ID</th>
    <th>Task</th>
    <th>Task Difficulty</th>
    <th>Assigned</th>
  </tr>

<?php 
forEach($result as $res){
    ?>
<tr>
    <td><?php echo $res['id']?></td>
    <td><?php  echo $res['task'] ?></td>
    <td><?php  echo $res['task_difficult']?></td>
    <td> <?php  echo $res['assigned_to']?></td>
    <td><a href="./modify.php?id=<?php echo $res['id']?>">Modify</a></td>
    <td><a href="./delete.php?id=<?php echo $res['id']?>">Delete</a></td>
  </tr>

<?php
}
?>
</table>

</body>
</html>