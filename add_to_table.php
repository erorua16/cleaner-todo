<?php include "connection.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="go_back">
<a href="index.php">
<img src="./src/images/go-back.png" alt="" style="height: 50px">
Go back
</a>

<!----------------------------------------------------------------ADD ITEMS TO: TO DO------------------------------------------------------------------------------->
<h1>ADD AN ITEM TO THE TABLE</h1>
<form method="post">
    <label>Task:</label><input type="text" name="task_name"/>
    <br>
    <br>
    <label>Task difficulty:</label><input type="number" name="difficulty_task"/>
    <br>
    <br>
    <label>Assigned to:</label><input type="text" name="name_assigned_to"/>
    <br>
    <br>
    <input type="submit" name="submit" value="Submit"/>
</form>

<?php 
$name_task            =    !empty ($_REQUEST["task_name"])           ?        $_REQUEST["task_name"]         : NULL;
$task_difficulty      =    !empty ($_REQUEST["difficulty_task"])     ?  (int) $_REQUEST["difficulty_task"]   : NULL;
$assigned             =    !empty ($_REQUEST["name_assigned_to"])    ?        $_REQUEST["name_assigned_to"]  : NULL;
if(!empty($name_task && $task_difficulty  && $assigned ) === true){
    $sql2 = $mysqli->prepare("INSERT INTO list_of_todo (task, assigned_to, task_difficult) VALUES (?, ?, ?)");
    // Assign variables
    $sql2->bind_param('ssi', $name_task, $assigned, $task_difficulty);
    $sql2->execute();
}
else {
    echo "Not added to table, make sure to input all values";
}
?>
</div>
</body>
</html>